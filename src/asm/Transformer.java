package thinkpadassist.asm;

import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import net.minecraft.launchwrapper.IClassNameTransformer;
import net.minecraft.launchwrapper.IClassTransformer;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.commons.RemappingClassAdapter;

import cpw.mods.fml.common.asm.transformers.deobf.FMLRemappingAdapter;
import cpw.mods.fml.relauncher.FMLLaunchHandler;

public class Transformer implements IClassTransformer, Opcodes {

    private static final String TARGET_CLASS_NAME = "net.minecraft.util.MouseHelper";
    
	@Override
	public byte[] transform(String name, String transformedName, byte[] bytes) {
        if (!FMLLaunchHandler.side().isClient() || !transformedName.equals(TARGET_CLASS_NAME))
        {
            // 処理対象外なので何もしない
            return bytes;
        }
        try
        {
            return replaceClass(bytes);
 
        }
        catch (Exception e)
        {
            throw new RuntimeException("failed : ThinkPadAssist: Transformer loading", e);
        }
	}
	   // 下記の想定で実装されています。
    // 対象クラスの bytes を ModifiedTargetClass.class ファイルに置き換える
    private byte[] replaceClass(byte[] bytes) throws IOException
    {
        ZipFile zf = null;
        InputStream zi = null;
 
        try
        {
            zf = new ZipFile(CorePlugin.location);
 
            // 差し替え後のファイルです。coremodのjar内のパスを指定します。
            ZipEntry ze = zf.getEntry("thinkpadassist/asm/ModifiedMouseHelper.class");
 
            if (ze != null)
            {
                zi = zf.getInputStream(ze);
                bytes = new byte[(int) ze.getSize()];
                zi.read(bytes);
            }
 
            return bytes;
        }
        finally
        {
            if (zi != null)
            {
                zi.close();
            }
 
            if (zf != null)
            {
                zf.close();
            }
        }
    }

}
