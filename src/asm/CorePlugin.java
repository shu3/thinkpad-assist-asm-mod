package thinkpadassist.asm;

import java.io.File;
import java.util.Map;
 
import cpw.mods.fml.relauncher.IFMLLoadingPlugin;
import cpw.mods.fml.relauncher.IFMLLoadingPlugin.TransformerExclusions;

@TransformerExclusions({"thinkpadassist.asm"})
public class CorePlugin implements IFMLLoadingPlugin {

	static File location;
	
	@Override
	public String[] getLibraryRequestClass() {
		return null;
	}

	@Override
	public String[] getASMTransformerClass() {
		return new String[]{"thinkpadassist.asm.Transformer"};
	}

	@Override
	public String getModContainerClass() {
		return "thinkpadassist.asm.ModContainer";
	}

	@Override
	public String getSetupClass() {
		return null;
	}

	@Override
	public void injectData(Map<String, Object> data) {
        if (data.containsKey("coremodLocation"))
        {
            location = (File) data.get("coremodLocation");
        }
	}

}
