package thinkpadassist.asm;

import java.util.Arrays;

import cpw.mods.fml.common.DummyModContainer;
import cpw.mods.fml.common.ModMetadata;

public class ModContainer extends DummyModContainer {
	public ModContainer() {
		super(new ModMetadata());

		// 他のModと区別するための一意なIDやmodの名前など、MODのメタデータを設定します。
		ModMetadata meta = getMetadata();

		meta.modId       = "ThinkPadAssistAsm";
		meta.name        = "ThinkPadAssistAsm";
		meta.version     = "0.0.1";
		meta.authorList  = Arrays.asList("shu3");
		meta.description = "";
		meta.url         = "";
		meta.credits     = "";
	}
}
